package com.yktkononov.solarsystem;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.yktkononov.solarsystem.screens.GameScreen;

public class SolarSystem extends Game {

	public static final int WIDTH = 800;
	public static final int HEIGHT = 480;
	public static final String TITLE = "Solar System";

	private GameScreen gs;
	
	@Override
	public void create () {
		this.gs = new GameScreen();
		setScreen(gs);
	}

}
