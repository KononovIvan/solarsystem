package com.yktkononov.solarsystem.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.yktkononov.solarsystem.SolarSystem;
import com.yktkononov.solarsystem.model.AstronomicalBody;
import com.yktkononov.solarsystem.model.Planet;
import com.yktkononov.solarsystem.model.World;

public class WorldRenderer {

    private ShapeRenderer renderer = new ShapeRenderer();
    private World world;
    private OrthographicCamera cam;

    public WorldRenderer(World world){
        this.world = world;
        cam = new OrthographicCamera();
        cam.setToOrtho(false, SolarSystem.WIDTH, SolarSystem.HEIGHT);
    }

    public void render(){
        Gdx.gl.glClearColor(0, 0, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        drawPlanets();
        drawStar();
    }

    private void drawStar(){
        drawBody(world.getStar());
    }

    private void drawPlanets(){
        for (Planet planet: world.getPlanets()) {
            drawBody(planet);
        }
    }

    private void drawBody(AstronomicalBody body){
        renderer.setProjectionMatrix(cam.combined);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(body.getColor());
        renderer.circle(body.getPosition().x, body.getPosition().y, body.getR());
        renderer.end();
    }
}
