package com.yktkononov.solarsystem.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.yktkononov.solarsystem.SolarSystem;
import com.yktkononov.solarsystem.model.Planet;
import com.yktkononov.solarsystem.model.World;
import com.yktkononov.solarsystem.view.WorldRenderer;

public class GameScreen implements Screen{

    private Stage stage;
    private World world;
    private WorldRenderer renderer;
    private Button button;
    private int width, height;
    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        Texture texture = new Texture(Gdx.files.internal("squarebutton.png"));
        Drawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
        button = new ImageButton(drawable);
        button.setSize(100, 100);
        stage.addActor(button);
        world = new World();
        renderer = new WorldRenderer(world);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                world.setActive(!world.isActive());
            }
        });
    }

    @Override
    public void render(float delta) {
        if(world.isActive()){
            for (Planet planet: world.getPlanets()) {
                //planet.getVelocity().x = planet.getSpeed();
                //planet.getVelocity().y = planet.getSpeed();
                planet.update(delta);
            }
        }

        renderer.render();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        button.setPosition(10,height/2f - button.getHeight()/2f);
        /*System.out.println(width + " x " + height);
        System.out.println("button pos: " + button.getX() + ", " + button.getY());
        System.out.println("sun pos: " + world.getStar().getPosition());*/
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
