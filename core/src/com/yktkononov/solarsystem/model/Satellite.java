package com.yktkononov.solarsystem.model;

import com.badlogic.gdx.math.Vector2;

public interface Satellite {
    AstronomicalBody getParent();
    Vector2 getVelocity();
    void update(float delta);
    float getSpeed();
    void setSpeed(float speed);
}
