package com.yktkononov.solarsystem.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public abstract class AstronomicalBody {

    private float R;
    private Vector2 position;
    private Color color;

    AstronomicalBody(float R, Vector2 position, Color color){
        this.R = R;
        this.position = position;
        this.color = color;
    }

    public float getR() {
        return R;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Color getColor() {
        return color;
    }

    public void setR(float r) {
        R = r;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
