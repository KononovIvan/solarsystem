package com.yktkononov.solarsystem.model;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;


public class Planet extends AstronomicalBody implements Satellite {

    private AstronomicalBody parent;
    private float distance; //distance from parent
    private Vector2 velocity = new Vector2();
    private float speed = 3f;
    private float alpha = 0f;

    public Planet(float R, Vector2 position, Color color, AstronomicalBody parent){
        super(R, position, color);
        this.parent = parent;
        this.distance = position.dst(parent.getPosition());
    }

    public Planet(float R, float distance, Color color, AstronomicalBody parent){
        super(R, new Vector2(parent.getPosition().x + distance, parent.getPosition().y), color);
        this.parent = parent;
        this.distance = distance;
    }

    @Override
    public AstronomicalBody getParent() {
        return parent;
    }

    @Override
    public Vector2 getVelocity() {
        return velocity;
    }

    @Override
    public void update(float delta) {

        //getPosition().mulAdd(velocity, delta);
        //System.out.println("alpha: " + alpha);
        getPosition().x = parent.getPosition().x + distance * (float)Math.cos(alpha);
        getPosition().y = parent.getPosition().y + distance * (float)Math.sin(alpha);
        alpha = (alpha + delta * speed) % (2 * (float)Math.PI);
    }

    @Override
    public float getSpeed() {
        return speed;
    }

    @Override
    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
