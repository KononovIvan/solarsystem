package com.yktkononov.solarsystem.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.yktkononov.solarsystem.SolarSystem;

public class Star extends AstronomicalBody {

    public Star(float R, Vector2 position, Color color){
        super(R, position, color);
    }

    public Star(float R, Vector2 position){
        this(R, position, Color.WHITE);
    }

    public Star(float R){
        this(R, new Vector2(SolarSystem.WIDTH/2f, SolarSystem.HEIGHT/2f));
    }

    public Star(){
        this(10);
    }
}
