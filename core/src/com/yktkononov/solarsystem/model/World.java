package com.yktkononov.solarsystem.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.yktkononov.solarsystem.SolarSystem;

public class World {

    private boolean active;
    private Array<Planet> planets = new Array<Planet>();
    private Star star;

    public World(){
        star = new Star(30);
        System.out.println(star.getPosition());
        star.setColor(Color.YELLOW);
        Planet planet1 = new Planet(15, 100, Color.CYAN, star);
        Planet planet2 = new Planet(10, 180, Color.FOREST, star);
        Planet planet3 = new Planet(5, 30, Color.GRAY, planet2);
        addPlanet(planet1);
        addPlanet(planet2);
        addPlanet(planet3);
        planet1.setSpeed(.5f);
        planet2.setSpeed(1f);
        planet3.setSpeed(5f);
        setActive(true);
    }

    public void addPlanet(Planet planet){
        planets.add(planet);
    }
    public void setStar(Star star) {
        this.star = star;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Star getStar() {
        return star;
    }

    public Array<Planet> getPlanets() {
        return planets;
    }

    public boolean isActive() {
        return active;
    }
}
