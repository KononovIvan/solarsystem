package com.yktkononov.solarsystem.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.yktkononov.solarsystem.SolarSystem;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = SolarSystem.WIDTH;
		config.height = SolarSystem.HEIGHT;
		config.title = SolarSystem.TITLE;
		new LwjglApplication(new SolarSystem(), config);
	}
}
